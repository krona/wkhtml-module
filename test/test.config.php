<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 5:18 PM
 */
return [
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/view',
        ],
        'template_map' => [
            'test.phtml'           => __DIR__ . '/view/test.phtml',
        ],
    ],
    'listeners' => [
        'RouteListener',
        'DispatchListener',
        'ViewManager',
        'SendResponseListener',
    ],
];
