<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 6:29 PM
 */
return array(
    'modules' => array(
        'Krona\CommonModule',
        'Krona\WKHTML',
    ),
    'module_listener_options' => array(
        'config_glob_paths' => array(
            '../../../config/autoload/{,*.}{global,local}.php',
            'test.config.php',
        ),
        'module_paths' => array(),
    ),
);
