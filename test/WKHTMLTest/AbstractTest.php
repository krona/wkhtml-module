<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 6:07 PM
 */

namespace Krona\WKHTMLTest;


use Doctrine\Common\Annotations\AnnotationRegistry;
use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Zend\ServiceManager\ServiceManager;

trait AbstractTest
{
    /** @var  ServiceManager */
    protected static $sm;

    public static function setUpBeforeClass()
    {
        self::$sm = Bootstrap::getServiceManager();
    }
}