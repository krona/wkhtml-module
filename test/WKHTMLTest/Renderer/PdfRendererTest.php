<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 6:12 PM
 */

namespace Krona\WKHTMLTest\Renderer;


use Krona\WKHTML\Renderer\PdfRenderer;
use Krona\WKHTML\View\Model\PdfModel;
use Krona\WKHTMLTest\AbstractTest;

class PdfRendererTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testPdfRender()
    {
        /** @var PdfRenderer $pdfRenderer */
        $pdfRenderer = self::$sm->get(PdfRenderer::class);
        $this->assertInstanceOf(PdfRenderer::class, $pdfRenderer);

        $view = new PdfModel();
        $view->setTemplate('test.phtml');
        $view->setOrientation(PdfModel::ORIENTATION_LANDSCAPE);
//        $view->setHeaderUrl('http://0.0.0.0:4000/header.html');

        $pdfRenderer->render($view);
    }
}
