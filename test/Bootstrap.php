<?php
/**
 * User: Alex Grand (alex.gandza@nixsolutions.com)
 * Date: 9/26/13
 * Time: 6:28 PM
 */

namespace Krona\WKHTMLTest;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Krona\CommonModule\Service\Accessor\PropertyAccessor;
use Krona\CommonModule\Service\Accessor\PropertyAccessorAwareInterface;
use Zend\EventManager\EventManager;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\Mvc\Service\ViewResolverFactory;
use Zend\Mvc\View\Http\ViewManager;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplatePathStack;
use Zend\View\ViewEvent;

error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

class Bootstrap
{
    protected static $serviceManager;
    protected static $config;
    protected static $bootstrap;

    protected static $managers = [
        'FilterManager',
        'ControllerLoader',
        'ControllerPluginManager',
        'InputFilterManager',
        'ViewHelperManager',
        'ValidatorManager',
    ];

    public static function init()
    {
        chdir(dirname(__DIR__));
        date_default_timezone_set('UTC');
        // Load the user-defined test configuration file, if it exists; otherwise, load
        if (is_readable(__DIR__ . '/TestConfig.php')) {
            $testConfig = include __DIR__ . '/TestConfig.php';
        } else {
            $testConfig = include __DIR__ . '/TestConfig.php.dist';
        }

        $global = array_merge(
            $testConfig,
            include __DIR__ . '/test.config.php'
        );
        $serviceManager = new ServiceManager(
            new ServiceManagerConfig()
        );
        AnnotationRegistry::registerLoader(function($name){
            return class_exists($name);
        });
        $serviceManager->setService('ApplicationConfig', $global);
        $serviceManager->get('ModuleManager')->loadModules();
        $serviceManager->addInitializer(function ($instance) use ($serviceManager){
            if ($instance instanceof PropertyAccessorAwareInterface) {
                /** @var PropertyAccessor $propertyAccessor */
                $propertyAccessor = $serviceManager->get(PropertyAccessor::class);
                $propertyAccessor->process($instance);
            }
        });
        foreach(static::$managers as $managerName) {
            /** @var AbstractPluginManager $manager */
            $manager = $serviceManager->get($managerName);
            $manager->addInitializer(function ($instance) use ($serviceManager){
                if ($instance instanceof PropertyAccessorAwareInterface) {
                    /** @var PropertyAccessor $propertyAccessor */
                    $propertyAccessor = $serviceManager->get(PropertyAccessor::class);
                    $propertyAccessor->process($instance);
                }
            });
        }
        $config                     = $serviceManager->get('Config');
        $listenersFromConfigService = isset($config['listeners']) ? $config['listeners'] : array();

        $listeners = array_unique($listenersFromConfigService);
        /** @var EventManager $eventManager */
        $eventManager = $serviceManager->get('EventManager');
        foreach ($listeners as $listener) {
            $eventManager->attach($serviceManager->get($listener));
        }

        $renderer = new PhpRenderer();
        $renderer->setHelperPluginManager($serviceManager->get('ViewHelperManager'));
        /** @var TemplatePathStack $resolver */
        $resolver = $renderer->resolver();
        $resolver->addPath(realpath('test/view'));

        $serviceManager->setService('ViewRenderer', $renderer);
        $serviceManager->setAlias('Zend\View\Renderer\PhpRenderer', 'ViewRenderer');

        static::$serviceManager = $serviceManager;
        static::$config = $global;
    }

    /**
     * @return ServiceManager
     */
    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    public static function getConfig()
    {
        return static::$config;
    }
}

Bootstrap::init();
