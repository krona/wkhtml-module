Krona WKHTML Module
==============
Krona WKHTML is an ZF2 Module for simplify working with PDF generation

## Install

### Composer

Install Composer [Composer](http://getcomposer.org):

```bash
$ curl -s https://getcomposer.org/installer | php
```

Add requirements to `composer.json`:

```yaml
{
    "require": {
        "krona/wkhtml-module": "dev-master"
    }
}
```

### Configuration

In your application config add module:

```php
<?php
return [
    // This should be an array of module namespaces used in the application.
    'modules' => [
        'Krona\CommonModule',
        'Krona\WKHTML',
        ...
    ],
    ...
],

```

If you want to use lib with ability to send PDF directly to user from Action register Strategy inside global config:

```php
'view_manager' => [
    'strategies' => [
        \Krona\WKHTML\Strategy\PdfStrategy::class,
    ],
],
```

Also you need to add config for placement and base URL for temporary location of HTML for example:

```php
    'krona' => [
        'wkhtml' => [
            'dir' => 'public/placement/',
            'base_url' => 'http://my.domain/placement/'
        ],
    ],
```

## Usage

### Simple use with sending PDF to user

```php
class SomeController extends AbstractController
{
    public function testAction()
    {
        //...
        $view = new PdfModel( //The same as ViewModel
            [
                
            ]
        );
        $view->setOrientation(PdfModel::ORIENTATION_LANDSCAPE);
        
        return $view;
    }
}
```