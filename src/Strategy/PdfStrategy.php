<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 4:59 PM
 */

namespace Krona\WKHTML\Strategy;


use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Krona\WKHTML\Renderer\PdfRenderer;
use Krona\WKHTML\View\Model\PdfModel;
use Zend\Http\PhpEnvironment\Response;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Strategy\PhpRendererStrategy;
use Zend\View\ViewEvent;

class PdfStrategy extends PhpRendererStrategy implements ServiceManagerAwareInterface
{
    /**
     * @Service(name="PdfRenderer")
     * @var PdfRenderer
     */
    protected $renderer;

    public function __construct()
    {

    }

    /**
     * Detect if we should use the JsonRenderer based on model type and/or
     * Accept header
     *
     * @param  ViewEvent $e
     * @return null|PdfRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if (!$model instanceof PdfModel) {
            // no JsonModel; do nothing
            return null;
        }

        // JsonModel found
        return $this->renderer;
    }

    /**
     * Inject the response with the JSON payload and appropriate Content-Type header
     *
     * @param  ViewEvent $e
     * @return void
     */
    public function injectResponse(ViewEvent $e)
    {
        $renderer = $e->getRenderer();
        if ($renderer !== $this->renderer) {
            // Discovered renderer is not ours; do nothing
            return;
        }

        $result   = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no JSON
            return;
        }

        // Populate response
        /** @var Response $response */
        $response = $e->getResponse();
        $response->setContent($result);
        $headers = $response->getHeaders();

        $contentType = 'application/pdf';

        $headers->addHeaderLine('content-type', $contentType);
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->renderer = $serviceManager->get(PdfRenderer::class);
    }
}