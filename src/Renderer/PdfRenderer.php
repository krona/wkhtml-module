<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 5:04 PM
 */

namespace Krona\WKHTML\Renderer;

use Krona\CommonModule\Service\Accessor\Mapping\Service;
use Krona\WKHTML\View\Model\PdfModel;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Renderer\RendererInterface;
use Zend\View\Resolver\ResolverInterface;

class PdfRenderer implements RendererInterface, ServiceManagerAwareInterface
{
    const NATIVE_WKHTML = 'native';
    const DEPENDENCY_WKHTML = 'dependency';

    /**
     * @Service(name="Config")
     * @var array
     */
    protected $config;

    /**
     * @Service(name="Zend\View\Renderer\PhpRenderer")
     * @var PhpRenderer
     */
    protected $phpRenderer;

    public function render($nameOrModel, $values = null)
    {
        $config = $this->config['krona']['wkhtml'];
        if (!file_exists($config['dir'])) {
            mkdir($config['dir']);
        }

        $fileName = uniqid('render');
        $htmlName = realpath($config['dir']) . DIRECTORY_SEPARATOR . $fileName . '.html';
        $pdfName = realpath($config['dir']) . DIRECTORY_SEPARATOR . $fileName . '.pdf';
        if ($config['type'] == self::DEPENDENCY_WKHTML) {
            $command = 'vendor/bin/wkhtmltopdf-' . $config['os'];
        } else {
            $command = $config['command'];
        }
        if ($nameOrModel instanceof PdfModel) {
            $command .= $nameOrModel->initCommand();
        }

        $body = $this->phpRenderer->render($nameOrModel, $values);
        file_put_contents($htmlName, $body);

        $command .= ' ' . $config['base_url'] . $fileName . '.html';
        $command .= ' ' . $pdfName;

        exec($command);

        $pdf = file_get_contents($pdfName);

        unlink($htmlName);
        unlink($pdfName);

        return $pdf;
    }

    /**
     * Return the template engine object
     *
     * Returns the object instance, as it is its own template engine
     *
     * @return $this
     */
    public function getEngine()
    {
        return $this;
    }

    /**
     * Set script resolver
     *
     * @param  ResolverInterface $resolver
     * @return $this
     * @throws
     */
    public function setResolver(ResolverInterface $resolver)
    {
        $this->phpRenderer->setResolver($resolver);
        return $this;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->phpRenderer = $serviceManager->get('Zend\View\Renderer\PhpRenderer');
        $this->config = $serviceManager->get('Config');
    }
}