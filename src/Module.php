<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 4:58 PM
 */

namespace Krona\WKHTML;


class Module
{
    public function getConfig()
    {
        return require_once __DIR__ . '/Resources/config/module.config.php';
    }
}