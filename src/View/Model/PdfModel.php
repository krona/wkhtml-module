<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 5:25 PM
 */

namespace Krona\WKHTML\View\Model;


use Zend\View\Model\ViewModel;

class PdfModel extends ViewModel
{
    const ORIENTATION_LANDSCAPE = 'landscape';
    const ORIENTATION_PORTRAIT = 'portrait';

    const ENCODING_UTF_8 = 'UTF-8';

    /**
     * PDF probably won't need to be captured into a
     * a parent container by default.
     *
     * @var string
     */
    protected $captureTo = null;

    /**
     * PDF is usually terminal
     *
     * @var bool
     */
    protected $terminate = true;

    /**
     * Orientation of PDF Document
     * @var string
     */
    protected $orientation = self::ORIENTATION_PORTRAIT;

    /**
     * Encoding of PDF Document
     * @var string
     */
    protected $encoding = self::ENCODING_UTF_8;

    /**
     * Header HTML url or false
     * @var bool|string
     */
    protected $headerUrl = false;

    /**
     * Footer HTML url or false
     * @var bool|string
     */
    protected $footerUrl = false;

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     * @return $this
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
        return $this;
    }

    /**
     * @return string
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     * @return $this
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
        return $this;
    }

    /**
     * @return bool|string
     */
    public function getHeaderUrl()
    {
        return $this->headerUrl;
    }

    /**
     * @param bool|string $headerUrl
     * @return $this
     */
    public function setHeaderUrl($headerUrl)
    {
        $this->headerUrl = $headerUrl;
        return $this;
    }

    /**
     * @return bool|string
     */
    public function getFooterUrl()
    {
        return $this->footerUrl;
    }

    /**
     * @param bool|string $footerUrl
     * @return $this
     */
    public function setFooterUrl($footerUrl)
    {
        $this->footerUrl = $footerUrl;
        return $this;
    }

    public function initCommand()
    {
        $command = ' --orientation ' . $this->getOrientation();
        $command .= ' --encoding ' . $this->getEncoding();
        if ($this->getHeaderUrl()) {
            $command .= ' --header-html ' . $this->getHeaderUrl();
        }
        if ($this->getFooterUrl()) {
            $command .= ' --footer-html ' . $this->getFooterUrl();
        }

        return $command;
    }
}