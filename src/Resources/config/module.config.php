<?php
/**
 * User: granted
 * Date: 2/3/15
 * Time: 4:57 PM
 */
return [
    'krona' => [
        'wkhtml' => [
            'type' => 'dependency', // or native
            'command' => 'DISPLAY=:99.0 wkhtmltopdf', //example of usage with native wkhtmltopdf
            'dir' => 'public/placement',
            'os' => 'amd64',
            'base_url' => 'http://0.0.0.0:4000/placement/'
        ],
    ],
    'service_manager' => [
        'invokables' => [
            \Krona\WKHTML\Strategy\PdfStrategy::class => \Krona\WKHTML\Strategy\PdfStrategy::class,
            \Krona\WKHTML\Renderer\PdfRenderer::class => \Krona\WKHTML\Renderer\PdfRenderer::class,
        ],
        'aliases' => [
            'PdfStrategy' => \Krona\WKHTML\Strategy\PdfStrategy::class,
            'PdfRenderer' => \Krona\WKHTML\Renderer\PdfRenderer::class,
        ],
    ],
];